# Objectives (6 weeks)

## 1. Host a website in the internet
   1. Create a dummy react app using node, (typescript, optional), vscode
   2. use gitlab for versioning.
   3. Using netlify, host it for free.
   4. Enable https
   5. Adding proper DNS entries, so that it is available under your domain/subdomain

## 2. API server as spring boot.
   1. API server with mysql as backend, hibernate as ORM, using spring data
   2. Writing api test cases using JUNIT, hamcrest
   
   
## 3. AWS as our cloud provider (free tier)
   1. EC2 ubuntu server as computing engine
   2. RDS mysql as backend
   3. nginx as reverse proxy
   4. Connect react site to api server, CORS issues

