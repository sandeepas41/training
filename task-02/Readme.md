# Task-01 Online accounts
> Create an account in the following sites. Each of them will be used in our upcoming tasks

    1. https://aws.amazon.com/
        - free tier is available for 12 months.
        - They might call you for verification, after account creation
    2. https://www.netlify.com/
    3. https://gitlab.com/
   
    