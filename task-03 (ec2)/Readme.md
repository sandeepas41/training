# Objectives 
> Create an instance with configuration of our choice.

> Login to the server using ssh

> install softwares in the server

> install nginx as web server and view it in browser.

> Refer the video links, if needed.


## 1. Create ec2 server (free tier)
   1. ssh to the remote server
   2. install nginx
   3. access default landing page from our browser
   4. terminate the instance


> Always remember to terminate your instances (not shutdown, TERMINATE) after use. Otherwise your card will be charged.



## 2.1 Installation of nginx
1. ssh to the instance.
2. Update your OS
   >sudo apt update 
3. install nginx    
    >sudo apt install nginx
4. By default nginx is started after installation.
5. Go to browser and access your ip.


## Reference
1. [Regions and availability zones](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)
2. [EC2 documentations](https://aws.amazon.com/ec2/?ec2-whats-new.sort-by=item.additionalFields.postDateTime&ec2-whats-new.sort-order=desc)
3. [EC2 develper docs](https://aws.amazon.com/ec2/getting-started/
)
4. [Connect Ec2 ubuntu instance from windows](https://www.mindbowser.com/how-to-connect-to-aws-ec2-ubuntu-gui-using-putty-from-the-windows-machine/
)


## Videos
1. ec2 creation [Google Drive](https://drive.google.com/file/d/1Cj8avp-ilPHj1HpV08RXPveHqHTKU4V6/view?usp=sharing)
2. nginx installation [Google Drive](https://drive.google.com/file/d/1ui0KgQBDrCdA-s72FTAZR_F4J40IFNIJ/view?usp=sharing)
3. ec2 termination [Google Drive](https://drive.google.com/file/d/1Se8Gjpkj13gRzsoCOOjMPW31uVpuDmVE/view?usp=sharing)


## Additonal 
1. Familiar with all the instnce types available in aws 
   >https://aws.amazon.com/ec2/instance-types/
2. Configuration of security groups and access restriction,importance of http https, ssh ports enabled during instance creation.
3. nginx
   >https://www.nginx.com/
4. Difference between instance shutdown and instance termination
5. Autoscaling, Elastic ip address, load balancers
   