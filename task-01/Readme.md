# Task-01 Preparation of our environment
> we'll use all the tools down the line. If you already installed them, upgrade it to the latest version.

>Theoretically everything we are planning is OS agnostic, or worst-case you will have alternatives.

>You are free to choose Ubuntu or windows

>Unless specified, you have to use the latest version of available softwares.

>All softwares will be free or have a free evaluation versions.

# Softwares

## Install Mysql
   
    1. [Download](https://www.mysql.com/downloads/)
    2. Who is the owner of Mysql?
    3. Mysql vs Oracle, pros and cons
    4. Start/Stop mysql using commandline
    5. Enable/Disable mysql from Operating System startup
    6. Login to mysql instance using root


## Install Mysql Workbench
### Level 01
    1. [Download](https://www.mysql.com/products/workbench/)
    2. Connect to mysql instance from UI
    3. Create multiple schemas with names schema_01, schema_01
    4. Create 2 tables in schema_01. schema_01_user, schema_01_address
    5. Create 2 tables in schema_02. schema_02_user, schema_02_address
    6. Every table should have a column ID, with BIGINT(20). Add 2-3 additional columns 
    7. Insert rows to all 4 tables.
    8. switch schemas in mysql workbench and run select queries

### Level 02
    1.  Alter any one table to have DATE, DATETIME and TIMESTAMP columns. [Referrence](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)
    2. Update values to these columsn, understand the differences
    3. SELECT query with where condition for <,>, =  to these columns
    4. Convert date columns to human readable string in SELECT query. (eg. 01-12-2021, 01 January, 2021)
    5. Find all rows, with <date_column> < CURRENT_DATE
    6. find all rows, with <date_time_column> less than CURRENT_DATE_AND_TIME

### Level 03
    1. find the default timezone of your mysql instance
    2. If your db is serving clients from all over the world, from a data center in MUMBAI (db is installed in MUMBAI), what will be your timzone ? 
        - use mumbai timezone, pros and cons
        - use some other timezone, pros and cons
    3.  With in a schema, write a query to find the table with maximum rows using query
    4.  Given an column name (say, postal_code), write a query to find all tables having that column name in your instance
    5.  Create a view, combining two tables in schema_01 (If you dont have a foriegn key, add one)
    6.  Add constraints to your tables. (UNIQUE, INDEX etc)
    7.  Given an constraint name (in a application, usually you will get the constraint name from application logs), write a query to find the table having that constraint name in your instance.

   
## Install git
    1. Learn the difference between git, github, gitlab and bitbucket.
    2. What is the default branch name in github/gitlab? (it is not master)
    3. Why this is changed from master? 
    4. Learn develop, feature branches. [Reference] (https://nvie.com/posts/a-successful-git-branching-model/)
   
## Install fork
    1. [Download] (https://git-fork.com/)
    2. If it is not available in your OS, download any alternatives
    3. Learn how to 
       1. commit
       2. merge
       3. revert
       4. sqaush
       5. cherrypick
       6. rebase
       7. reset
       8. stash
    4. Only evaluation version of this software (unlimited) is free. 
   
## Install nvm (node version manager)
    1. Why install node using nvm, instead of directly installing node.
    2. Verify nvm is installed correctly
    3. Go through the commands/options of nvm to 
        - list all node versions available (remote)
        -  list all node versions installed locally
        - use a particular node version

## Install node using nvm
    1. install the latest version
    2. Verify the installation
   
## install java 16
    1. install java 16 from any provider, openjdk or oracle
    2. openjdk vs oracle, pros and cons
    3. set PATH, verify the installation

## install spring tool suite
    1. install
    2. spring tool suite vs eclipse, pros and cons
   
## install VS code
    1. install

## Optional
    1. postman for api testing
    2. insomnia for api testing, alternative to postman
    3. Anydesk for remote desktop, alternative to teamviewer